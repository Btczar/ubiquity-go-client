package main

import (
	"fmt"
	"os"
	"time"

	"gitlab.com/Blockdaemon/ubiquity/ubiquity-go-client/examples"

	ubiquityWs "gitlab.com/Blockdaemon/ubiquity/ubiquity-go-client/v1/pkg/ws"
)

/**
Subscribing to Websocket channel 'ubiquity.txs'. See https://app.blockdaemon.com/docs/ubiquity#ubiquity-web-sockets-api

Env variables:
	1) UBI_ACCESS_TOKEN - required, Ubiquity API Access Token
	2) UBI_PLATFORM - optional, platform e.g. ethereum (bitcoin by default)
	3) UBI_NETWORK - optional, network (mainnet by default)
*/

const (
	loopDuration = 10 * time.Minute
)

func main() {
	// Access token is required
	accessToken := os.Getenv("UBI_ACCESS_TOKEN")
	if accessToken == "" {
		panic(fmt.Errorf("env variable 'UBI_ACCESS_TOKEN' must be set"))
	}

	// Websocket URL is required
	wsUrl := os.Getenv("UBI_WS_URL")
	if wsUrl == "" {
		panic(fmt.Errorf("env variable 'UBI_WS_URL' must be set"))
	}

	// Use ubiquity.PlatformsAPI.GetPlatforms(ctx _context.Context) to fetch all supported platforms
	// See examples/platforms-overview
	var pl string
	if pl = os.Getenv("UBI_PLATFORM"); pl == "" {
		pl = examples.ProtocolBTC
	}

	var network string
	if network = os.Getenv("UBI_NETWORK"); network == "" {
		network = examples.NetworkTestnet
	}

	wsClient, err := ubiquityWs.NewClient(&ubiquityWs.Config{
		Platform:     pl,
		Network:      network,
		APIKey:       accessToken,
		WebsocketURL: wsUrl,
	})
	if err != nil {
		panic(fmt.Errorf("failed to create a WS client: %v", err))
	}
	defer func() {
		if err := wsClient.Close(); err != nil {
			panic(fmt.Errorf("failed to close WS client: %v", err))
		} // This will automatically unsubscribe all subscriptions
	}()

	subID, txs, err := wsClient.SubscribeTxs(nil)
	if err != nil {
		panic(fmt.Errorf("failed to subscribe to txs: %v", err))
	}
	// It's a good practice to unsubscribe if you plan to run WS client for a long time (which is not a case here)
	//defer wsClient.UnsubscribeTxs(subID)

	startedAt := time.Now()
	finishAt := startedAt.Add(loopDuration)
	fmt.Printf("Subscribed to %s txs under subID #%s\n", pl, subID)
	for {
		select {
		case b, ok := <-txs:
			if !ok {
				// The subscription would close if we close a client or get disconnected
				fmt.Println("The subscription was closed")
				return
			}
			fmt.Printf(
				"Received a tx #%s with hash %s and event count %d\n",
				b.GetId(), b.GetId(), len(b.GetEvents()),
			)
		case <-time.After(finishAt.Sub(time.Now())):
			fmt.Println("Ending the subscription loop.")
			return
		}
	}
}
