# BlockIdentifierPage

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Total** | Pointer to **int32** | Number of items in block identifiers | [optional] 
**Items** | Pointer to [**[]BlockIdentifier**](BlockIdentifier.md) |  | [optional] 
**Continuation** | Pointer to **NullableInt32** | Token to get the next page | [optional] 

## Methods

### NewBlockIdentifierPage

`func NewBlockIdentifierPage() *BlockIdentifierPage`

NewBlockIdentifierPage instantiates a new BlockIdentifierPage object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewBlockIdentifierPageWithDefaults

`func NewBlockIdentifierPageWithDefaults() *BlockIdentifierPage`

NewBlockIdentifierPageWithDefaults instantiates a new BlockIdentifierPage object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetTotal

`func (o *BlockIdentifierPage) GetTotal() int32`

GetTotal returns the Total field if non-nil, zero value otherwise.

### GetTotalOk

`func (o *BlockIdentifierPage) GetTotalOk() (*int32, bool)`

GetTotalOk returns a tuple with the Total field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTotal

`func (o *BlockIdentifierPage) SetTotal(v int32)`

SetTotal sets Total field to given value.

### HasTotal

`func (o *BlockIdentifierPage) HasTotal() bool`

HasTotal returns a boolean if a field has been set.

### GetItems

`func (o *BlockIdentifierPage) GetItems() []BlockIdentifier`

GetItems returns the Items field if non-nil, zero value otherwise.

### GetItemsOk

`func (o *BlockIdentifierPage) GetItemsOk() (*[]BlockIdentifier, bool)`

GetItemsOk returns a tuple with the Items field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetItems

`func (o *BlockIdentifierPage) SetItems(v []BlockIdentifier)`

SetItems sets Items field to given value.

### HasItems

`func (o *BlockIdentifierPage) HasItems() bool`

HasItems returns a boolean if a field has been set.

### GetContinuation

`func (o *BlockIdentifierPage) GetContinuation() int32`

GetContinuation returns the Continuation field if non-nil, zero value otherwise.

### GetContinuationOk

`func (o *BlockIdentifierPage) GetContinuationOk() (*int32, bool)`

GetContinuationOk returns a tuple with the Continuation field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetContinuation

`func (o *BlockIdentifierPage) SetContinuation(v int32)`

SetContinuation sets Continuation field to given value.

### HasContinuation

`func (o *BlockIdentifierPage) HasContinuation() bool`

HasContinuation returns a boolean if a field has been set.

### SetContinuationNil

`func (o *BlockIdentifierPage) SetContinuationNil(b bool)`

 SetContinuationNil sets the value for Continuation to be an explicit nil

### UnsetContinuation
`func (o *BlockIdentifierPage) UnsetContinuation()`

UnsetContinuation ensures that no value is present for Continuation, not even an explicit nil

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


