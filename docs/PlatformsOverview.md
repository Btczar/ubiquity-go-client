# PlatformsOverview

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Platforms** | Pointer to [**[]PlatformsOverviewPlatforms**](PlatformsOverviewPlatforms.md) | List of items each describing a pair of supported platform and network. | [optional] 

## Methods

### NewPlatformsOverview

`func NewPlatformsOverview() *PlatformsOverview`

NewPlatformsOverview instantiates a new PlatformsOverview object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewPlatformsOverviewWithDefaults

`func NewPlatformsOverviewWithDefaults() *PlatformsOverview`

NewPlatformsOverviewWithDefaults instantiates a new PlatformsOverview object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetPlatforms

`func (o *PlatformsOverview) GetPlatforms() []PlatformsOverviewPlatforms`

GetPlatforms returns the Platforms field if non-nil, zero value otherwise.

### GetPlatformsOk

`func (o *PlatformsOverview) GetPlatformsOk() (*[]PlatformsOverviewPlatforms, bool)`

GetPlatformsOk returns a tuple with the Platforms field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPlatforms

`func (o *PlatformsOverview) SetPlatforms(v []PlatformsOverviewPlatforms)`

SetPlatforms sets Platforms field to given value.

### HasPlatforms

`func (o *PlatformsOverview) HasPlatforms() bool`

HasPlatforms returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


