# FeeEstimateEstimatedFees

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Fast** | Pointer to **interface{}** |  | [optional] 
**Medium** | Pointer to **interface{}** |  | [optional] 
**Slow** | Pointer to **interface{}** |  | [optional] 

## Methods

### NewFeeEstimateEstimatedFees

`func NewFeeEstimateEstimatedFees() *FeeEstimateEstimatedFees`

NewFeeEstimateEstimatedFees instantiates a new FeeEstimateEstimatedFees object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewFeeEstimateEstimatedFeesWithDefaults

`func NewFeeEstimateEstimatedFeesWithDefaults() *FeeEstimateEstimatedFees`

NewFeeEstimateEstimatedFeesWithDefaults instantiates a new FeeEstimateEstimatedFees object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetFast

`func (o *FeeEstimateEstimatedFees) GetFast() interface{}`

GetFast returns the Fast field if non-nil, zero value otherwise.

### GetFastOk

`func (o *FeeEstimateEstimatedFees) GetFastOk() (*interface{}, bool)`

GetFastOk returns a tuple with the Fast field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetFast

`func (o *FeeEstimateEstimatedFees) SetFast(v interface{})`

SetFast sets Fast field to given value.

### HasFast

`func (o *FeeEstimateEstimatedFees) HasFast() bool`

HasFast returns a boolean if a field has been set.

### SetFastNil

`func (o *FeeEstimateEstimatedFees) SetFastNil(b bool)`

 SetFastNil sets the value for Fast to be an explicit nil

### UnsetFast
`func (o *FeeEstimateEstimatedFees) UnsetFast()`

UnsetFast ensures that no value is present for Fast, not even an explicit nil
### GetMedium

`func (o *FeeEstimateEstimatedFees) GetMedium() interface{}`

GetMedium returns the Medium field if non-nil, zero value otherwise.

### GetMediumOk

`func (o *FeeEstimateEstimatedFees) GetMediumOk() (*interface{}, bool)`

GetMediumOk returns a tuple with the Medium field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMedium

`func (o *FeeEstimateEstimatedFees) SetMedium(v interface{})`

SetMedium sets Medium field to given value.

### HasMedium

`func (o *FeeEstimateEstimatedFees) HasMedium() bool`

HasMedium returns a boolean if a field has been set.

### SetMediumNil

`func (o *FeeEstimateEstimatedFees) SetMediumNil(b bool)`

 SetMediumNil sets the value for Medium to be an explicit nil

### UnsetMedium
`func (o *FeeEstimateEstimatedFees) UnsetMedium()`

UnsetMedium ensures that no value is present for Medium, not even an explicit nil
### GetSlow

`func (o *FeeEstimateEstimatedFees) GetSlow() interface{}`

GetSlow returns the Slow field if non-nil, zero value otherwise.

### GetSlowOk

`func (o *FeeEstimateEstimatedFees) GetSlowOk() (*interface{}, bool)`

GetSlowOk returns a tuple with the Slow field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSlow

`func (o *FeeEstimateEstimatedFees) SetSlow(v interface{})`

SetSlow sets Slow field to given value.

### HasSlow

`func (o *FeeEstimateEstimatedFees) HasSlow() bool`

HasSlow returns a boolean if a field has been set.

### SetSlowNil

`func (o *FeeEstimateEstimatedFees) SetSlowNil(b bool)`

 SetSlowNil sets the value for Slow to be an explicit nil

### UnsetSlow
`func (o *FeeEstimateEstimatedFees) UnsetSlow()`

UnsetSlow ensures that no value is present for Slow, not even an explicit nil

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


