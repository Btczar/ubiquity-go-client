# NativeCurrency

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**AssetPath** | **string** | Asset path of transferred currency | 
**Symbol** | Pointer to **string** | Currency symbol | [optional] 
**Name** | Pointer to **string** | Name of currency | [optional] 
**Decimals** | Pointer to **int32** | Decimal places right to the comma | [optional] 
**Type** | **string** |  | [default to "native"]

## Methods

### NewNativeCurrency

`func NewNativeCurrency(assetPath string, type_ string, ) *NativeCurrency`

NewNativeCurrency instantiates a new NativeCurrency object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewNativeCurrencyWithDefaults

`func NewNativeCurrencyWithDefaults() *NativeCurrency`

NewNativeCurrencyWithDefaults instantiates a new NativeCurrency object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetAssetPath

`func (o *NativeCurrency) GetAssetPath() string`

GetAssetPath returns the AssetPath field if non-nil, zero value otherwise.

### GetAssetPathOk

`func (o *NativeCurrency) GetAssetPathOk() (*string, bool)`

GetAssetPathOk returns a tuple with the AssetPath field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAssetPath

`func (o *NativeCurrency) SetAssetPath(v string)`

SetAssetPath sets AssetPath field to given value.


### GetSymbol

`func (o *NativeCurrency) GetSymbol() string`

GetSymbol returns the Symbol field if non-nil, zero value otherwise.

### GetSymbolOk

`func (o *NativeCurrency) GetSymbolOk() (*string, bool)`

GetSymbolOk returns a tuple with the Symbol field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSymbol

`func (o *NativeCurrency) SetSymbol(v string)`

SetSymbol sets Symbol field to given value.

### HasSymbol

`func (o *NativeCurrency) HasSymbol() bool`

HasSymbol returns a boolean if a field has been set.

### GetName

`func (o *NativeCurrency) GetName() string`

GetName returns the Name field if non-nil, zero value otherwise.

### GetNameOk

`func (o *NativeCurrency) GetNameOk() (*string, bool)`

GetNameOk returns a tuple with the Name field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetName

`func (o *NativeCurrency) SetName(v string)`

SetName sets Name field to given value.

### HasName

`func (o *NativeCurrency) HasName() bool`

HasName returns a boolean if a field has been set.

### GetDecimals

`func (o *NativeCurrency) GetDecimals() int32`

GetDecimals returns the Decimals field if non-nil, zero value otherwise.

### GetDecimalsOk

`func (o *NativeCurrency) GetDecimalsOk() (*int32, bool)`

GetDecimalsOk returns a tuple with the Decimals field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDecimals

`func (o *NativeCurrency) SetDecimals(v int32)`

SetDecimals sets Decimals field to given value.

### HasDecimals

`func (o *NativeCurrency) HasDecimals() bool`

HasDecimals returns a boolean if a field has been set.

### GetType

`func (o *NativeCurrency) GetType() string`

GetType returns the Type field if non-nil, zero value otherwise.

### GetTypeOk

`func (o *NativeCurrency) GetTypeOk() (*string, bool)`

GetTypeOk returns a tuple with the Type field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetType

`func (o *NativeCurrency) SetType(v string)`

SetType sets Type field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


